// File structure
data class File(val name: String, var content: String)

// Directory structure
class Directory(val name: String) {
    val files = mutableMapOf<String, File>()
    val subDirectories = mutableMapOf<String, Directory>()
}

// File System class
class InMemoryFileSystem {
    private val root = Directory("root")

    // Helper function to traverse the path and get the parent directory
    private fun traversePath(path: String): Directory {
        val parts = path.split("/").filter { it.isNotEmpty() }
        var current = root

        // Traverse up to the second last element for parent directories
        for (part in parts.dropLast(1)) {
            current = current.subDirectories[part] ?: throw Exception("Directory not found: $path")
        }

        return current
    }

    // Create a new file
    fun createFile(path: String, content: String) {
        val parentDirectory = traversePath(path)
        val fileName = path.split("/").last()

        if (parentDirectory.files.containsKey(fileName)) {
            throw Exception("File already exists: $path")
        }

        parentDirectory.files[fileName] = File(fileName, content)
    }

    // Read a file
    fun readFile(path: String): String {
        val parentDirectory = traversePath(path)
        val fileName = path.split("/").last()

        return parentDirectory.files[fileName]?.content
            ?: throw Exception("File not found: $path")
    }

    // Write to a file
    fun writeFile(path: String, content: String) {
        val parentDirectory = traversePath(path)
        val fileName = path.split("/").last()

        val file = parentDirectory.files[fileName] ?: throw Exception("File not found: $path")
        file.content = content
    }

    // Create a new directory
    fun createDirectory(path: String) {
        val parentDirectory = traversePath(path)
        val dirName = path.split("/").last()

        if (parentDirectory.subDirectories.containsKey(dirName)) {
            throw Exception("Directory already exists: $path")
        }

        parentDirectory.subDirectories[dirName] = Directory(dirName)
    }

    // List the contents of a directory
    fun listDirectory(path: String): List<String> {
        val targetDirectory = traversePath(path)
        val files = targetDirectory.files.keys
        val subDirectories = targetDirectory.subDirectories.keys

        return (files + subDirectories).sorted().toList()
    }

    // Delete a file
    fun deleteFile(path: String) {
        val parentDirectory = traversePath(path)
        val fileName = path.split("/").last()

        parentDirectory.files.remove(fileName) ?: throw Exception("File not found: $path")
    }

    // Delete a directory
    fun deleteDirectory(path: String) {
        val parentDirectory = traversePath(path)
        val dirName = path.split("/").last()

        parentDirectory.subDirectories.remove(dirName) ?: throw Exception("Directory not found: $path")
    }
}

// Test the InMemoryFileSystem
fun main() {
    val fileSystemTester = InMemoryFileSystem()

    // Create directories and files
    fileSystemTester.createDirectory("/documents")
    fileSystemTester.createFile("/documents/file1.txt", "Hello, This is a content in this test file!")

    // Read file
    val content = fileSystemTester.readFile("/documents/file1.txt")
    println("Content of file1.txt: $content")

    // Write to a file
    fileSystemTester.writeFile("/documents/file1.txt", "Removing original text and Updated content with this value.")
    println("Updated content of file1.txt: ${fileSystemTester.readFile("/documents/file1.txt")}")

    // List directory
    val contents = fileSystemTester.listDirectory("/documents")
    println("Contents of /documents: $contents")

    // Delete file
    fileSystemTester.deleteFile("/documents/file1.txt")
    println("Contents of /documents after deletion: ${fileSystemTester.listDirectory("/documents")}")

    // Delete directory
    fileSystemTester.deleteDirectory("/documents")
    println("Contents of / after directory deletion: ${fileSystemTester.listDirectory("/")}")
}
