package codes

data class BinaryTreeNode(
    val value: Int,
    var left: BinaryTreeNode? = null,
    var right: BinaryTreeNode? = null
)

// Binary Tree Class represents a Binary Tree

class BinaryTree {
    private var root: BinaryTreeNode? = null

    // add a new value to the binary tree
    fun add(value: Int) {
        root = addRecursive(root, value)
    }

    private fun addRecursive(current: BinaryTreeNode?, value: Int): BinaryTreeNode {
        if(current == null) {
            return BinaryTreeNode(value)
        }

        when {
            value < current.value -> {
                current.left = addRecursive(current.left, value)
            }
            value > current.value -> {
                current.right = addRecursive(current.right, value)
            }
        }
        return current
    }

    //In order traversal: left, root, right
    fun traverseInOrder(node: BinaryTreeNode? = root) {
        if(node != null) {
            traverseInOrder(node.left)
            print(" " + node.value)
            traverseInOrder(node.right)
        }
    }

    //Pre order traversal: root, left, right
    fun traversePreOrder(node: BinaryTreeNode? = root) {
        if(node != null) {
            print(" " + node.value)
            traversePreOrder(node.left)
            traversePreOrder(node.right)
        }
    }

    // post order traversal: left, right, root
    fun traversePostOrder(node: BinaryTreeNode? = root) {
        if(node != null) {
            traversePostOrder(node.left)
            traversePostOrder(node.right)
            print(" " + node.value)
        }
    }

    // search for a value in the binary tree
    fun containsNode(value: Int) : Boolean {
        var current: BinaryTreeNode? = root // start with the root node
        while (current != null) {
            current = when {
                value == current.value -> return true // found the value
                value < current.value -> current.left // move to left subtree
                else -> current.right // move to the right subtree
            }
        }

        return false // value not found
    }
}

fun main() {
    val tree = BinaryTree()

    // adding nodes to the binary tree
    val valuesToAdd = listOf(10,5,15,3,7,12,6,13,18,1,2,4)
    println("Adding values to the tree: $valuesToAdd")
    valuesToAdd.forEach { tree.add(it) }

    print("\n In-Order Traversal:")
    tree.traverseInOrder()

    print("\n Pre-Order Traversal:")
    tree.traversePreOrder()

    print("\n Post-Order Traversal:")
    tree.traversePostOrder()
    println("\n")

    // Check for specific values in the tree
    val valuesToCheck = listOf(5, 14, 18, 20)
    valuesToCheck.forEach { value ->
        val contains = tree.containsNode(value)
        println("Tree contains $value: $contains")
    }
}

