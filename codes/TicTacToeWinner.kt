package codes

// Function to determine if there is a tic-tac-toe winner
fun checkTicTacToeWinner(board: List<List<Char>>): Char {
    // Function to check if three characters are the same and not empty
    fun allEqual(a: Char, b: Char, c: Char): Boolean {
        return a == b && b == c && a != ' '
    }

    // Check rows for a winner
    for (row in board) {
        if (allEqual(row[0], row[1], row[2])) {
            return row[0] // Return the winning character
        }
    }

    // Check columns for a winner
    for (i in 0..2) {
        if (allEqual(board[0][i], board[1][i], board[2][i])) {
            return board[0][i] // Return the winning character
        }
    }

    // Check diagonals for a winner
    if (allEqual(board[0][0], board[1][1], board[2][2])) {
        return board[0][0] // Return the winning character
    }

    // Check anti-diagonal
    if (allEqual(board[0][2], board[1][1], board[2][0])) {
        return board[0][2] // Return the winning character
    }

    // If no winner, return a space character
    return ' '
}

// Test the function with sample boards
fun main() {
    // Example 1: Horizontal winner (first row)
    val board1 = listOf(
        listOf('X', 'X', 'X'),
        listOf('O', 'O', ' '),
        listOf(' ', ' ', ' ')
    )

    // Example 2: Vertical winner (first column)
    val board2 = listOf(
        listOf('O', 'X', 'X'),
        listOf('O', 'X', ' '),
        listOf('O', ' ', ' ')
    )

    // Example 3: Diagonal winner
    val board3 = listOf(
        listOf('X', 'O', ' '),
        listOf('O', 'X', 'O'),
        listOf(' ', ' ', 'X')
    )

    // Example 4: Anti-diagonal winner
    val board4 = listOf(
        listOf('X', 'O', 'O'),
        listOf('X', 'O', ' '),
        listOf('O', ' ', 'X')
    )

    // Example 5: No winner
    val board5 = listOf(
        listOf('X', 'O', 'X'),
        listOf('O', 'X', 'O'),
        listOf('O', 'X', ' ')
    )

    val boards = listOf(board1, board2, board3, board4, board5)

    for ((index, board) in boards.withIndex()) {
        val winner = checkTicTacToeWinner(board)
        if (winner == ' ') {
            println("Board ${index + 1}: No winner")
        } else {
            println("Board ${index + 1}: Winner is: $winner")
        }
    }
}
