# Cricut Assessment

- Coding for each solution are in the codes folder

## Explanation of Binary Tree 

### Overview
- BinaryTree file contains code, which demonstrates basic binary search tree functionality, including adding nodes,
different types of tree traversals, and searching for a node with a specific value

#### 1. BinaryTreeNode Data class:
- Represents a node in the binary tree with an integer value and optional left and right child nodes

#### 2. BinaryTree class:
- Implements a binary search tree(BST) with methods to add nodes, traverse the tree in different orders and check if a value exists
in the tree

#### 3. Methods in BinaryTree class:
- `add(value: Int)`: Adds a new node to the binary tree by comparing the value to the current node and recursively placing it in the
correct position (left for smaller values, right for larger values)
- `traverseInOrder(node: BinaryTreeNode?)`: Performs an In-Order Traversal (left -> root -> right)
- `traversePreOrder(node: BinaryTreeNode?)`: Performs an Pre-Order Traversal (root -> left -> right)
- `traverseInOrder(node: BinaryTreeNode?)`: Performs an In-Order Traversal (left -> right -> root)
- `containsNode(value: Int)`: Searches for a specific value in the tree and returns `true` if found, `false` otherwise

***

## Explanation of In Memory File System

### Overview
- The In Memory File system design uses a Hierarchical structure with directories and files. The hash maps in directories ensure
efficient operations, providing O(1) complexity for common operations like insertions, lookups, and deletions. Path traversal is
implemented with O(n) complexity, where `n` represents the number of components in the path.
- This combination of data structures and algorithms creates a flexible and efficient in-memory file system that is capable
of performing wide range of file system operations with reliability and performance in mind.

### Data Structures

#### 1. File
 - **Type:** Data Class
 - **Attributes:** 
    - `name`: Represents the name of the file
    - `content`: Stores the file content as a `String`
 - **Properties**:
    - As a data class, it automatically generates `equals`, `hashcode`, and `toString`, which are essential for comparing files, creating readable output, and has-based storage
#### 2. Directory
- **Type:**  Class
- **Attributes:**
    - `name`: Name of a directory
    - `files`: A mutable map `MutableMap<String,File>` containing files by their names
    - `subDirectories`: A mutable map `MutableMap<String, Directory>` containing subdirectories by their names
- **Properties**:
    - uses hash maps for quick lookups, insertions, and deletions of files and subdirectories
- **Advantages**:
    - Efficient management of files and subdirectories with O(1) operations in most cases
- #### 3. InMemoryFileSystem
- **Type:**  Class
- **Attributes:**
    - `root`: The top-level `Directory` that represents the root of the file system
- **Properties**:
    - Contains methods to create, read, write and delete files and directories
- **Complexity**:
    - path traversal has O(n) complexity, where `n` is the number of components in the path
    - most file and directory operations are O(1) due to the use of hash maps
    - Hierarchical structure allows flexible path-based navigation


### Algorithms

#### 1. Path Traversal
- **Implementation:**
    - uses `split` to break down the path into its components
    - Iterates through the components to navigate to the desired directory
- **Complexity**:
    - O(n), where `n` is the number of components in the path. This complexity accounts for traversing the hierarchy
- **Properties:**
    - Handles both absolute and relative paths
    - Error Handling for Nonexistent directories or files
#### 2. File Operations
- **Implementation:**
    - `createFile`, `readFile`, `writeFile`, `deleteFile`
    - Involves path traversal to find the parent directory, then operates on the `files` map
- **Complexity**:
    - Generally O(1) for map-based operations (insertions, lookups, deletions)
- **Properties**:
    - Ensure unique file names in a given directory
    - proper error handling for file existence and nonexistence
#### 3. Directory Operations
- **Implementation:**
    - `createDirectory`, `listDirectory`, `deleteDirectory`
    - Operates on the `subDirectories` map after path traversal
- **Complexity**:
    - Similar to file operations, O(1) for most operations due to hash map use
- **Properties**:
    - Allows hierarchical structure with nested directories
    - Manages unique directory names and provided listing of contents
- **Error Handling**:
    - Handles cases where directories or files do not exist
    - Throws exceptions when creating duplicates

***

## Explanation of Tic Tac Win

### Overview
- TicTacToeWinner file contains code, which checks for tic-tac-toe winners in 3X3 grid and demonstrates the logic
for checking rows, columns, and diagonals for winning conditions

### `checkTicTacToeWinner` Functions
- This function checks a 3X3 tic-tac-toe board to determine if there is a winner
- It uses a nested function `allEqual(a: Char, b: Char, c: Char)` to check if three characters are the same and not empty
- The function checks the following for a winning condition
    - Rows - It iterates through each row and checks if all three elements are the same and not empty
    - Columns - It iterates through the columns and checks the same condition
    - Diagonals - It checks the main diagonal and the anti-diagonal for equality
- If a winning condition is met, the function returns the winning character `X` or `O`. If no winner is found then it returns a space character `''`.